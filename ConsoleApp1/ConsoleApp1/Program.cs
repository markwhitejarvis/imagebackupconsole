﻿using ConsoleApp1.Extension;
using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteStratTXT();
            /**
             * 讀取 DB 連線參數
             */
            DB_Parameter _p = new DB_Parameter
            {
                DataSource = ConfigurationManager.AppSettings["DataSource"],
                InitialCatalog = ConfigurationManager.AppSettings["InitialCatalog"],
                UserId = ConfigurationManager.AppSettings["UserId"],
                Password = ConfigurationManager.AppSettings["Password"],
                PersistSecurityInfo = ConfigurationManager.AppSettings["PersistSecurityInfo"],
            };

            // DB 連線參數整合
            string conStr = GetConStr(_p);
            int DataCountTemp;
            /**
* 讀取系統參數
*/
            Parameter _pter = new Parameter
            {
                OriginalImagePath = Directory.GetCurrentDirectory() + "\\OriginalImagePath\\" + ConfigurationManager.AppSettings["OriginalImageName"],
                ExportPath = Directory.GetCurrentDirectory() + "\\ExportPath\\",                       // 改放在執行區的資料夾
                CouponNo = ConfigurationManager.AppSettings["CouponNo"],
                DataCount = int.TryParse((ConfigurationManager.AppSettings["DataCount"]), out DataCountTemp) ? DataCountTemp : 0,
                CouponName = ConfigurationManager.AppSettings["CouponName"],
            };
            GoLog("[(絕對路徑)原圖路徑為]        【" + _pter.OriginalImagePath + "】");
            GoLog("[(絕對路徑)產出檔案的路徑為]  【" + _pter.ExportPath + "】");
            GoLog("[要複製的 CouponNo 為]        【" + _pter.CouponNo + "】");
            GoLog("[要複製的 筆數 為]            【" + _pter.DataCount + "】");
            GoLog("[Coupon Name 字串基準 為]     【" + _pter.CouponName + "】");

            GoLog("是否繼續執行 【輸入1:是】；【輸入其他：否】\n");
            var xxx = Console.ReadLine();
            int inputemp = 0;

            if (!Int32.TryParse(xxx, out inputemp))
            {
                GoLog("未輸入1(確認OK)，故系統無法繼續執行...");
                ExitSystem("");
            }

            GoLog("處理中請稍待......................................");

            //參數檢核
            CheckParameter(_pter);

            // 1. 執行複製 DB coupon資料
            List<string> fileNameList = ProcessInsertCouponSQL(conStr, _pter);

            //比數大於0時，做 檔案複製
            if (fileNameList.Count > 0)
            {
                // 2. 做圖片大量複製
                ProcessImage(_pter, fileNameList);
            }

            ExitSystem("");
        }

        /// <summary>
        /// 組合DB 連線字串
        /// </summary>
        /// <param name="_p"></param>
        /// <returns></returns>
        private static string GetConStr(DB_Parameter _p)
        {
            SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder
            {
                InitialCatalog = _p.InitialCatalog, // 資料庫名稱
                DataSource = _p.DataSource,         // 資料庫來源位址
                UserID = _p.UserId,                 // 管理者
                Password = _p.Password,             // 管理者密碼
                IntegratedSecurity = false,         //表示與 Windows 機制整合，False : 表示使用 DB 的管理者帳密
                //TrustServerCertificate = false,
                //MultipleActiveResultSets = true
                PersistSecurityInfo = Convert.ToBoolean(_p.PersistSecurityInfo),
            };
            var conStr = sqlBuilder.ToString();
            GoLog("[連線字串]        【" + conStr + "】");
            return conStr;
        }

        /// <summary>
        /// 可擴充，參數檢查
        /// </summary>
        /// <param name="_pter"></param>
        private static void CheckParameter(Parameter _pter)
        {
            if (_pter.DataCount == 0)
            {
                GoLog("預備複製的筆數參數 = 0  (有可能填了非數字)，故無法複製。");
                ExitSystem("");
            }
            if (_pter.DataCount > 99999999)
            {
                GoLog("預備複製筆數參數 > 99999999，故無法複製。");
                ExitSystem("");
            }
            if (_pter.CouponName.Length > 42)
            {
                GoLog("Coupon Name 參數資料長度 > 42，故無法複製。");
                ExitSystem("");
            }
            if (_pter.CouponNo.Length != 13)
            {
                GoLog("Coupon No 參數資料長度 != 13，為無效CouponNo，故無法複製。");
                ExitSystem("");
            }

            //要檢查日期參數的話，可能要先replace  .  -  / 這樣
        }

        /// <summary>
        /// 處理 insert coupon data
        /// </summary>
        /// <param name="conStr">DB 連線字串</param>
        /// <param name="_pter">系統參數</param>
        /// <returns>圖檔名陣列</returns>
        private static List<string> ProcessInsertCouponSQL(string conStr, Parameter _pter)
        {
            List<string> couponPicList = new List<string>();            // 圖檔名陣列，最後return 此陣列來決定是否要複製圖片
            CPN_Coupon_M couponM_Ori = null;                            // 原 Coupon_M 主檔
            List<CPN_CouQty_M> couQtyM_Ori = new List<CPN_CouQty_M>();  // 原 CPN_CouQty_M 檔

            List<CPN_Coupon_M> couponM_Copy = new List<CPN_Coupon_M>(); // 待複製 CPN_CouQty_M 檔
            List<CPN_CouQty_M> couQtyM_Copy = new List<CPN_CouQty_M>(); // 待複製 CPN_CouQty_M 檔

            try
            {
                // 1.撈原 CPN_Coupon_M
                // 2.撈原 CPN_CouQty_M
                GetCouponMDataList(conStr, _pter, ref couponM_Ori, ref couQtyM_Ori);

                if (couponM_Ori == null)
                {
                    GoLog("以此 CouponNo: " + _pter.CouponNo + "；沒有查詢到Coupon資料，故無法複製。");
                    ExitSystem("");
                }
                Console.WriteLine("\n");
                // 整理insert物件
                for (int i = 1; i <= _pter.DataCount; i++)
                {
                    DateTime now = DateTime.Now;

                    // 3.1 拿取號機 號碼--執行SP
                    string couponNoresult = string.Empty;
                    GetCouponNo(conStr, ref couponNoresult);

                    // 4.1 串好參數 CPN_Coupon_M
                    GetCouponMList(now, _pter.CouponName, i, couponNoresult, ref couponM_Ori, ref couponPicList, ref couponM_Copy);

                    // 4.2 串好參數 CPN_CouQty_M
                    if (couQtyM_Ori.Count > 0)
                    {
                        GetCouQtyMList(now, couponNoresult, couQtyM_Ori, ref couQtyM_Copy);
                    }
                }

                Console.WriteLine("\n");

                // 5.insert CPN_Coupon_M
                if (couponM_Copy.Count > 0)
                {
                    if (couponM_Copy.Count > 5000)
                    {
                        DataTable couponM_Copy_DataTable = ToDataTable<CPN_Coupon_M>(couponM_Copy);

                        if (BulkCopy(conStr, couponM_Copy_DataTable, "erp.CPN_Coupon_M"))
                        {
                            GoLog(string.Format("[BulkCopyCouponM]-總共寫入資料庫 CPN_Coupon_M 表：{0} 筆", couponM_Copy.Count));
                        }
                        else
                        {
                            GoLog("[BulkCopyCouponM]-INSERT INTO erp.CPN_Coupon_M 失敗，故無法複製。");
                            ExitSystem("");
                        }
                    }
                    else
                    {
                        InsertCouponM(conStr, couponM_Copy);
                    }
                }
                else
                {
                    GoLog("couponM_Copy 資料為 0 筆，故無法複製");
                    ExitSystem("");
                }
                // 6.insert CPN_CouQty_M
                if (couponM_Copy.Count > 0 && couQtyM_Copy.Count > 0)
                {
                    if (couQtyM_Copy.Count > 5000)
                    {
                        DataTable couQtyM_Copy_DataTable = ToDataTable<CPN_CouQty_M>(couQtyM_Copy);

                        if (BulkCopy(conStr, couQtyM_Copy_DataTable, "erp.CPN_CouQty_M"))
                        {
                            GoLog(string.Format("[BulkCopyCouQtyM]-總共寫入資料庫 CPN_CouQty_M 表：{0} 筆", couQtyM_Copy.Count));
                        }
                        else
                        {
                            GoLog("[BulkCopyCouQtyM]-INSERT INTO erp.CPN_CouQty_M 失敗，故無法複製。");
                            ExitSystem("");
                        }
                    }
                    else
                    {
                        InsertCouQtyM(conStr, couQtyM_Copy);
                    }
                }
                else
                {
                    GoLog(string.Format("couQtyM_Copy 資料為 0 筆，故無法複製"));
                    //ExitSystem("");
                }
            }
            catch (Exception ex)
            {
                GoLog(string.Format("異常訊息：{0} ;\nInnerException.Message：{1}", ex.Message, ex.InnerException.Message));
                ExitSystem("");
            }

            return couponPicList;
        }
        #region --Log 相關 --
        /// <summary>
        /// 同時處理寫log
        /// 跟顯示console
        /// </summary>
        /// <param name="message"></param>
        private static void GoLog(string message)
        {
            Console.WriteLine(message);
            WriteAppendTXT(message);
        }

        /// <summary>
        /// 開始寫log
        /// </summary>
        /// <param name="content">文字內容</param>
        private static void WriteStratTXT()
        {
            if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\log\\"))
            {
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\log\\");    //不存在就建立目錄
            }
            DateTime now = DateTime.Now;
            using (FileStream fs = new FileStream(Directory.GetCurrentDirectory() + "\\log\\log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt", FileMode.Append))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine("/※※※※※※※※※※※※※※ Start Execute====== " + now + " ※※※※※※※※※※※※※※/\n");
                }
            }
        }

        /// <summary>
        /// 結束寫log
        /// </summary>
        private static void WriteEndTXT()
        {
            if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\log\\"))
            {
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\log\\");    //不存在就建立目錄
            }
            DateTime now = DateTime.Now;
            using (FileStream fs = new FileStream(Directory.GetCurrentDirectory() + "\\log\\log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt", FileMode.Append))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine("/※※※※※※※※※※※※※※※ End Execute====== " + now + " ※※※※※※※※※※※※※※/\n");
                }
            }
        }

        /// <summary>
        /// 接續在同個文件寫檔
        /// </summary>
        /// <param name="content">文字內容</param>
        private static void WriteAppendTXT(string content)
        {
            if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\log\\"))
            {
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\log\\");    //不存在就建立目錄
            }
            DateTime now = DateTime.Now;
            using (FileStream fs = new FileStream(Directory.GetCurrentDirectory() + "\\log\\log_" + now.ToString("yyyy-MM-dd") + ".txt", FileMode.Append))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(content);
                    sw.WriteLine("---------------------------------------" + "DateTime:" + now.ToString("yyyy/MM/dd HH:mm:ss.fff") + "---------------------------------------------------\n");
                }
            }
        }
        #endregion --Log 相關 --

        #region --DB Service--

        /// <summary>
        /// 取得 原始，要複製的 CPN_Coupon M 及 CPN_CouQty_M 資料列表
        /// </summary>
        /// <param name="conStr"></param>
        /// <param name="_pter"></param>
        /// <param name="couponM_Ori"></param>
        /// <param name="couQtyM_Ori"></param>
        private static void GetCouponMDataList(string conStr, Parameter _pter, ref CPN_Coupon_M couponM_Ori, ref List<CPN_CouQty_M> couQtyM_Ori)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT a.[CouponNo],a.[CouponName],a.[CouponType],a.[MoneyType],a.[CouponMoney],a.[PubDateS],a.[PubDateE],a.[CasDateS],");
            sb.Append("a.[CasDateE],a.[LimitType],a.[LimitQty],a.[SafeQtyM],a.[KeepQty],a.[AgainQty],a.[SafeQtyS],a.[QtyType],");
            sb.Append("a.[Psample],a.[Ustore],a.[Prn1],a.[Prn2],a.[Prn3],a.[Prn4],a.[Prn5],a.[Prn6],a.[Prn7],a.[Prn8],");
            sb.Append("a.[Memo],a.[UrgentDate],a.[CreateDate],a.[CreateID],a.[DataDate],a.[UserID],a.[PosPrint],a.[FileName] ");
            sb.Append("FROM erp.CPN_Coupon_M a ");
            sb.Append("WHERE a.CouponNo = @CouponNo ");
            sb.Append("SELECT b.[Serial],b.[CouponNo],b.[StoreID],b.[OriQty],b.[SafeQty],b.[UsedQty],");
            sb.Append("b.[CreateDate],b.[CreateID],b.[DataDate],b.[UserID] ");
            sb.Append("FROM erp.CPN_CouQty_M b ");
            sb.Append("WHERE b.CouponNo = @CouponNo ");

            using (var cn = new SqlConnection(conStr))
            {
                //一次執行多組查詢，分別取回結果
                var multi = cn.QueryMultiple(sb.ToString(), new { CouponNo = DapperExtension.ToChar(_pter.CouponNo, 13) });

                couponM_Ori = multi.Read<CPN_Coupon_M>().FirstOrDefault();
                couQtyM_Ori = multi.Read<CPN_CouQty_M>().ToList();

                GoLog(string.Format("CouponNo：{0} / 狀態：{1}。", _pter.CouponNo, couponM_Ori != null ? "有撈到資料" : "無資料"));
                GoLog(string.Format("CouQtyM 筆數為 {0} 筆", couQtyM_Ori.Count()));
            }
        }

        /// <summary>
        /// 取號 CouponNo
        /// // exec erp.PBACGetNo @tranNo, @tranId output
        /// </summary>
        /// <param name="conStr"></param>
        /// <param name="tranNo"></param>
        /// <param name="couponNoresult"></param>
        private static void GetCouponNo(string conStr, ref string couponNoresult)
        {
            string tranNo = "10"; // char(2)  給呼叫couponNo的取號機使用
            using (var conn = new SqlConnection(conStr))
            {
                // 設定參數
                var parameters = new DynamicParameters();
                parameters.Add("@tranNo", tranNo, DbType.String, ParameterDirection.Input);
                parameters.Add("@tranNewId", "", DbType.String, ParameterDirection.InputOutput);
                conn.Execute("erp.PBACGetNo", parameters, commandType: CommandType.StoredProcedure);
                couponNoresult = parameters.Get<string>("@tranNewId");
            }

            if (couponNoresult == "")
            {
                GoLog("沒有取到 erp.PBACGetNo 資料，無法複製。");
                ExitSystem("");
            }
            else
            {
                GoLog(string.Format("已取得新 CouponNo = {0}。", couponNoresult));
            }
        }

        /// <summary>
        /// 取得 CouponM 集合，待存
        /// </summary>
        /// <param name="now">現在時間</param>
        /// <param name="couponNameParameter">couponName參數值</param>
        /// <param name="i">迴圈</param>
        /// <param name="couponNo">couponNo券號</param>
        /// <param name="couponM_Ori">原資料</param>
        /// <param name="couponPicList">coupon圖檔名列表</param>
        /// <param name="couponM_Copy">最後要儲存的 CPN_CouponM 集合列表</param>
        private static void GetCouponMList(DateTime now, string couponNameParameter, int i, string couponNo, ref CPN_Coupon_M couponM_Ori, ref List<string> couponPicList, ref List<CPN_Coupon_M> couponM_Copy)
        {
            // 3.2 用tranId 組成 檔名
            //組完後放進 couponPicList
            string fileName = couponNo + '_' + now.ToString("yyyyMMddHHmmss") + ".bmp";
            couponPicList.Add(fileName);

            // 3.3 coupon名稱，看參數有沒有設
            // 有設的話，就是參數加上後八碼流水號
            // 沒設的話，就是複製的名稱加八碼流水號
            string couponName = (string.IsNullOrWhiteSpace(couponNameParameter) ? couponM_Ori.CouponName : couponNameParameter) + i.ToString("00000000");

            // 4.1 串好參數 CPN_Coupon_M
            var tempcouponM = new CPN_Coupon_M
            {
                CouponNo = couponNo,      //
                CouponName = couponName,        //
                FileName = fileName,            //
                CreateDate = now,               //
                DataDate = now,                 //
                CouponType = couponM_Ori.CouponType,
                MoneyType = couponM_Ori.MoneyType,
                CouponMoney = couponM_Ori.CouponMoney,
                PubDateS = couponM_Ori.PubDateS,
                PubDateE = couponM_Ori.PubDateE,
                CasDateS = couponM_Ori.CasDateS,
                CasDateE = couponM_Ori.CasDateE,
                LimitType = couponM_Ori.LimitType,
                LimitQty = couponM_Ori.LimitQty,
                SafeQtyM = couponM_Ori.SafeQtyM,
                KeepQty = couponM_Ori.KeepQty,
                AgainQty = couponM_Ori.AgainQty,
                SafeQtyS = couponM_Ori.SafeQtyS,
                QtyType = couponM_Ori.QtyType,
                Psample = couponM_Ori.Psample,
                Prn1 = couponM_Ori.Prn1,
                Prn2 = couponM_Ori.Prn2,
                Prn3 = couponM_Ori.Prn3,
                Prn4 = couponM_Ori.Prn4,
                Prn5 = couponM_Ori.Prn5,
                Prn6 = couponM_Ori.Prn6,
                Prn7 = couponM_Ori.Prn7,
                Prn8 = couponM_Ori.Prn8,
                Memo = couponM_Ori.Memo,
                UrgentDate = couponM_Ori.UrgentDate,
                CreateID = couponM_Ori.CreateID,
                UserID = couponM_Ori.UserID,
                PosPrint = couponM_Ori.PosPrint,
            };
            couponM_Copy.Add(tempcouponM);
        }

        /// <summary>
        /// 取得 CPN_CouQty_M 集合，待存
        /// </summary>
        /// <param name="now">現在時間</param>
        /// <param name="couponNo">couponNo券號</param>
        /// <param name="couQtyM_Ori">原資料</param>
        /// <param name="couQtyM_Copy">最後要儲存的 CPN_CouQty_M 集合列表</param>
        private static void GetCouQtyMList(DateTime now, string couponNo, List<CPN_CouQty_M> couQtyM_Ori, ref List<CPN_CouQty_M> couQtyM_Copy)
        {
            List<CPN_CouQty_M> tempCouQtyMList = new List<CPN_CouQty_M>();
            tempCouQtyMList.AddRange(couQtyM_Ori);

            tempCouQtyMList = (from aaa in tempCouQtyMList
                               select new CPN_CouQty_M
                               {
                                   CouponNo = couponNo,
                                   CreateDate = now,
                                   CreateID = aaa.CreateID,
                                   UserID = aaa.UserID,
                                   DataDate = now,
                                   SafeQty = aaa.SafeQty,
                                   OriQty = aaa.OriQty,
                                   UsedQty = aaa.UsedQty,
                                   StoreID = aaa.StoreID,
                               }).ToList();
            couQtyM_Copy.AddRange(tempCouQtyMList);
        }

        /// <summary>
        /// 寫入 CPN_Coupon_M
        /// </summary>
        /// <param name="conStr">連線字串</param>
        /// <param name="couponM_Copy">要寫入的集合</param>
        private static void InsertCouponM(string conStr , List<CPN_Coupon_M> couponM_Copy) 
        {
            int _erow = 0;
            StringBuilder sb1 = new StringBuilder();
            sb1.Append("INSERT INTO erp.CPN_Coupon_M ([CouponNo],[CouponName],[CouponType],[MoneyType],[CouponMoney],[PubDateS],[PubDateE],");
            sb1.Append("[CasDateS],[CasDateE],[LimitType],[LimitQty],[SafeQtyM],[KeepQty],[AgainQty],[SafeQtyS],[QtyType],[Psample],[Ustore],");
            sb1.Append("[Prn1],[Prn2],[Prn3],[Prn4],[Prn5],[Prn6],[Prn7],[Prn8],[Memo],[UrgentDate],[CreateDate],[CreateID],[DataDate],[UserID],[PosPrint],[FileName])");
            sb1.Append("VALUES ");
            sb1.Append("(@CouponNo,@CouponName,@CouponType,@MoneyType,@CouponMoney,@PubDateS,@PubDateE, ");
            sb1.Append("@CasDateS,@CasDateE,@LimitType,@LimitQty,@SafeQtyM,@KeepQty,@AgainQty,@SafeQtyS,@QtyType,@Psample,@Ustore,");
            sb1.Append("@Prn1,@Prn2,@Prn3,@Prn4,@Prn5,@Prn6,@Prn7,@Prn8,@Memo,@UrgentDate,@CreateDate,@CreateID,@DataDate,@UserID,@PosPrint,@FileName)");
            string sqlCommand = sb1.ToString();
            using (SqlConnection conn = new SqlConnection(conStr))
            {
                conn.Open();
                using (SqlTransaction trans = conn.BeginTransaction())
                {
                    _erow = conn.Execute(sqlCommand, couponM_Copy, transaction: trans);
                    trans.Commit();
                }
            }
            if (_erow > 0)
            {
                GoLog(string.Format("總共寫入資料庫 CPN_Coupon_M 表： {0} 筆", _erow));
            }
            else
            {
                GoLog("INSERT INTO erp.CPN_Coupon_M 失敗，故無法複製。");
                ExitSystem("");
            }
        }

        /// <summary>
        /// 寫入 CPN_CouQty_M
        /// </summary>
        /// <param name="conStr">連線字串</param>
        /// <param name="couQtyM_Copy">要寫入的集合</param>
        private static void InsertCouQtyM(string conStr, List<CPN_CouQty_M> couQtyM_Copy)
        {
            int _erow = 0;

            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO [erp].[CPN_CouQty_M] ");
            sb.Append("([CouponNo],[StoreID],[OriQty],[SafeQty],[UsedQty],[CreateDate],[CreateID],[DataDate],[UserID]) ");
            sb.Append("VALUES ");
            sb.Append("(@CouponNo,@StoreID,@OriQty,@SafeQty,@UsedQty,@CreateDate,@CreateID,@DataDate,@UserID)");
            string sqlCommand = sb.ToString();
            using (SqlConnection conn = new SqlConnection(conStr))
            {
                conn.Open();
                using (SqlTransaction trans = conn.BeginTransaction())
                {
                    _erow = conn.Execute(sqlCommand, couQtyM_Copy, transaction: trans);
                    trans.Commit();
                }
            }
            if (_erow > 0)
            {
                GoLog(string.Format("總共寫入資料庫 [CPN_CouQty_M] 表： {0} 筆", _erow));
            }
            else
            {
                GoLog("INSERT INTO [CPN_CouQty_M] 失敗，故無法複製。");
                ExitSystem("");
            }
        }

        /// <summary>
            /// 大量批次新增
            /// </summary>
            /// <param name="conStr"></param>
            /// <param name="dtSource"></param>
            /// <param name="dataTableName"></param>
            /// <returns></returns>
        private static bool BulkCopy(string conStr, DataTable dtSource,string dataTableName)
        {
            using (SqlConnection connection = new SqlConnection(conStr))
            {
                connection.Open();

                SqlTransaction sqlTrans = connection.BeginTransaction();

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.KeepIdentity, sqlTrans))
                {

                    DataTable dt = dtSource;
                    bulkCopy.DestinationTableName = dataTableName;
                    bulkCopy.BatchSize = 1000;
                    bulkCopy.BulkCopyTimeout = 60;

                    try
                    {
                        bulkCopy.WriteToServer(dt);
                    }
                    catch (Exception ex)
                    {
                        GoLog(string.Format("異常訊息：{0} ;\nInnerException.Message：{1}；\n", ex.Message, ex.InnerException.Message));

                        sqlTrans.Rollback();
                        return false;
                    }

                    sqlTrans.Commit();
                    return true;

                }
            }
        }

        /// <summary>
        /// List 轉 DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        private static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        #endregion --DB Service--

        #region --複製圖片--

        /// <summary>
        /// 處理複製圖片
        /// </summary>
        /// <param name="_pter">路徑參數</param>
        /// <param name="fileNameList">圖片檔名列表</param>
        private static void ProcessImage(Parameter _pter, List<string> fileNameList)
        {
            // 確定原圖使否存在及抓到
            if (!File.Exists(_pter.OriginalImagePath))
            {
                GoLog("原圖檔不存在。");
                ExitSystem("");
            }
            // 確定匯出目錄是否存在，不存在就建立
            if (!Directory.Exists(_pter.ExportPath))
            {
                Directory.CreateDirectory(_pter.ExportPath);//不存在就建立目錄
            }

            try
            {
                // 複製
                foreach (var item in fileNameList)
                {
                    File.Copy(_pter.OriginalImagePath, _pter.ExportPath + "/" + item, true);
                }
                GoLog("******檔案已複製完成****** \n ");
            }
            catch (Exception ex)
            {
                GoLog("複製檔案出現錯誤。\n" + ex.Message);
                ExitSystem("");
            }
        }

        #endregion --複製圖片--

        #region --系統 相關--

        /// <summary>
        /// 離開系統，基本型
        /// </summary>
        /// <param name="message">客製化訊息一</param>
        private static void ExitSystem(string message)
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                Console.WriteLine(message);
            }
            WriteEndTXT();
            Console.WriteLine("程式結束。按enter鍵離開 ");
            Console.Read();
            Environment.Exit(0);
        }

        #endregion --系統 相關--

        #region -- Class --
        /// <summary>
        /// DB 參數
        /// </summary>
        public class DB_Parameter
        {
            public string DataSource { get; set; }
            public string InitialCatalog { get; set; }
            public string UserId { get; set; }
            public string Password { get; set; }
            public string PersistSecurityInfo { get; set; }
        }

        public class Parameter
        {
            public string OriginalImagePath { get; set; }
            public string ExportPath { get; set; }

            public string CouponNo { get; set; }

            public int DataCount { get; set; }

            public string CouponName { get; set; }
        }

        /// <summary>
        /// Coupon主檔
        /// </summary>
        public class CPN_Coupon_M
        {
            /// <summary>
            /// Coupo券主檔編號
            /// </summary>
            [Key]
            [Display(Name = "Coupo券主檔編號")]
            public string CouponNo { get; set; }

            /// <summary>
            /// Coupon名稱
            /// </summary>
            [StringLength(50)]
            [Display(Name = "Coupon名稱")]
            public string CouponName { get; set; }

            /// <summary>
            /// Coupon券別
            /// </summary>
            [StringLength(1)]
            [Display(Name = "Coupon券別")]
            public string CouponType { get; set; }

            /// <summary>
            /// 面額類別
            /// </summary>
            [StringLength(1)]
            [Display(Name = "面額類別")]
            public string MoneyType { get; set; }

            /// <summary>
            /// 額度
            /// </summary>
            //[DecimalPrecision(7, 2)]
            [Display(Name = "額度")]
            public decimal? CouponMoney { get; set; }

            /// <summary>
            /// 發行期間起日
            /// </summary>
            [StringLength(10)]
            [Display(Name = "發行期間起日")]
            public string PubDateS { get; set; }

            /// <summary>
            /// 發行期間迄日
            /// </summary>
            [StringLength(10)]
            [Display(Name = "發行期間迄日")]
            public string PubDateE { get; set; }

            /// <summary>
            /// 兌換期間起日
            /// </summary>
            [StringLength(10)]
            [Display(Name = "兌換期間起日")]
            public string CasDateS { get; set; }

            /// <summary>
            /// 兌換期間迄日
            /// </summary>
            [StringLength(10)]
            [Display(Name = "兌換期間迄日")]
            public string CasDateE { get; set; }

            /// <summary>
            /// 總數量限制
            /// </summary>
            [StringLength(1)]
            [Display(Name = "總數量限制")]
            public string LimitType { get; set; }

            /// <summary>
            /// 限量張數
            /// </summary>
            //[DecimalPrecision(9, 0)]
            [Display(Name = "限量張數")]
            public decimal? LimitQty { get; set; }

            /// <summary>
            /// 總部低水位
            /// </summary>
            //[DecimalPrecision(2, 0)]
            [Display(Name = "總部低水位")]
            public decimal? SafeQtyM { get; set; }

            /// <summary>
            /// 總部保留量
            /// </summary>
            //[DecimalPrecision(9, 0)]
            [Display(Name = "總部保留量")]
            public decimal? KeepQty { get; set; }

            /// <summary>
            /// 門市再次取量張數
            /// </summary>
            //[DecimalPrecision(9, 0)]
            [Display(Name = "門市再次取量張數")]
            public decimal? AgainQty { get; set; }

            /// <summary>
            /// 門市低水位
            /// </summary>
            //[DecimalPrecision(2, 0)]
            [Display(Name = "門市低水位")]
            public decimal? SafeQtyS { get; set; }

            /// <summary>
            /// 配量方式
            /// </summary>
            [StringLength(1)]
            [Display(Name = "配量方式")]
            public string QtyType { get; set; }

            /// <summary>
            /// 套用樣版
            /// </summary>
            [StringLength(1)]
            [Display(Name = "套用樣版")]
            public string Psample { get; set; }

            /// <summary>
            /// 使用店別
            /// </summary>
            [StringLength(50)]
            [Display(Name = "使用店別")]
            public string Ustore { get; set; }

            /// <summary>
            /// 第一列
            /// </summary>
            [StringLength(100)]
            [Display(Name = "第一列")]
            public string Prn1 { get; set; }

            /// <summary>
            /// 第二列
            /// </summary>
            [StringLength(100)]
            [Display(Name = "第二列")]
            public string Prn2 { get; set; }

            /// <summary>
            /// 第三列
            /// </summary>
            [StringLength(100)]
            [Display(Name = "第三列")]
            public string Prn3 { get; set; }

            /// <summary>
            /// 第四列
            /// </summary>
            [StringLength(100)]
            [Display(Name = "第四列")]
            public string Prn4 { get; set; }

            /// <summary>
            /// 第五列
            /// </summary>
            [StringLength(100)]
            [Display(Name = "第五列")]
            public string Prn5 { get; set; }

            /// <summary>
            /// 第六列
            /// </summary>
            [StringLength(100)]
            [Display(Name = "第六列")]
            public string Prn6 { get; set; }

            /// <summary>
            /// 第七列
            /// </summary>
            [StringLength(100)]
            [Display(Name = "第七列")]
            public string Prn7 { get; set; }

            /// <summary>
            /// 第八列
            /// </summary>
            [StringLength(100)]
            [Display(Name = "第八列")]
            public string Prn8 { get; set; }

            /// <summary>
            /// 備註
            /// </summary>
            [StringLength(500)]
            [Display(Name = "備註")]
            public string Memo { get; set; }

            /// <summary>
            /// 緊急主檔下傳日
            /// </summary>
            [StringLength(10)]
            [Display(Name = "緊急主檔下傳日")]
            public string UrgentDate { get; set; }

            /// <summary>
            /// 建立日期
            /// </summary>
            [Display(Name = "建立日期")]
            public DateTime CreateDate { get; set; }

            /// <summary>
            /// 建立人員
            /// </summary>
            [StringLength(8)]
            [Display(Name = "建立人員")]
            public string CreateID { get; set; }

            /// <summary>
            /// 更新日期
            /// </summary>
            [Display(Name = "更新日期")]
            public DateTime DataDate { get; set; }

            /// <summary>
            /// 更新人員
            /// </summary>
            [StringLength(8)]
            [Display(Name = "更新人員")]
            public string UserID { get; set; }

            /// <summary>
            /// Pos預設列印
            /// </summary>
            [StringLength(1)]
            [Display(Name = "Pos預設列印")]
            public string PosPrint { get; set; }

            /// <summary>
            /// Coupon圖片檔名
            /// </summary>
            [StringLength(100)]
            [Display(Name = "Coupon圖片檔名")]
            public string FileName { get; set; }
        }

        /// <summary>
        /// Coupon配量主檔
        /// </summary>
        public class CPN_CouQty_M
        {
            /// <summary>
            /// 資料流水號
            /// </summary>
            [Key, Column(Order = 0)]
            [Required]
            [Display(Name = "資料流水號")]
            public long Serial { get; set; }

            /// <summary>
            /// Coupo券主檔編號
            /// </summary>
            [StringLength(13)]
            [Display(Name = "Coupo券主檔編號")]
            public string CouponNo { get; set; }

            /// <summary>
            /// 門市店號
            /// </summary>
            [StringLength(8)]
            [Display(Name = "門市店號")]
            public string StoreID { get; set; }

            /// <summary>
            /// 主檔分配量
            /// </summary>
            //[DecimalPrecision(9, 0)]
            [Display(Name = "主檔分配量")]
            public decimal? OriQty { get; set; }

            /// <summary>
            /// 保留分配量
            /// </summary>
            //[DecimalPrecision(9, 0)]
            [Display(Name = "保留分配量")]
            public decimal? SafeQty { get; set; }

            /// <summary>
            /// 已使用分配量
            /// </summary>
            //[DecimalPrecision(9, 0)]
            [Display(Name = "已使用分配量")]
            public decimal? UsedQty { get; set; }

            /// <summary>
            /// 建立日期
            /// </summary>
            [Display(Name = "建立日期")]
            public DateTime CreateDate { get; set; }

            /// <summary>
            /// 建立人員
            /// </summary>
            [StringLength(8)]
            [Display(Name = "建立人員")]
            public string CreateID { get; set; }

            /// <summary>
            /// 更新日期
            /// </summary>
            [Display(Name = "更新日期")]
            public DateTime DataDate { get; set; }

            /// <summary>
            /// 更新人員
            /// </summary>
            [StringLength(8)]
            [Display(Name = "更新人員")]
            public string UserID { get; set; }
        }

        #endregion --Class--







        /*
         
      }
      */

        /*
       /// <summary>
       /// 寫出 Log 文字檔
       /// </summary>
       /// <param name="title">標題；可不填</param>
       /// <param name="content">文字內容</param>
       private static void WriteTXT(string title, string content)
       {
           if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\log\\"))
           {
               Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\log\\");    //不存在就建立目錄
           }
           DateTime now = DateTime.Now;
           var nowStr = DateTime.Now.ToString("yyyyMMddHHmmssfff");
           using (StreamWriter sw = new StreamWriter(Directory.GetCurrentDirectory() + "\\log\\Error_log_" + nowStr + ".txt"))   //小寫TXT     
           {
               if (!string.IsNullOrWhiteSpace(title))
               {
                   sw.WriteLine(title + "\n");
               }
               // Add some text to the file.
               sw.WriteLine(content + "\n");
               sw.WriteLine("----------------------------------------------------------------------------------------------------");
               // Arbitrary objects can also be written to the file.
               sw.Write("The date is: ");
               sw.WriteLine(now + "\n");
           }
       }
       */
    }
}
